F8ures - 8chan's first Greasemonkey Script
======

###TO ADD THE SCRIPT : https://greasyfork.org/scripts/5221-f8ures

A dumb Greasemonkey script which adds extra (read: autistic) features on 8chan!

So far, it supports : 
 - Return, Catalog, Bottom/Top links in threads.

I'm hoping to add more features, so if you wanna contribute, go ahead!

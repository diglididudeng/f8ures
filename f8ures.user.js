/*
    <F8ures - 4chan Features for 8chan>
    Copyright (C) 2013, 2014  F8ures

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    http://opensource.org/licenses/GPL-3.0
*/

if (typeof $ == 'undefined') {
	var $ = unsafeWindow.jQuery;
}

// #### Global variables. ####
// URL of the current page on the site. 
// Gives this : 
// 0: a blank, 
// 1: the board, 
// 2: a dir name or the filename, 
// 3: filename containing thread number, only included if index 2 contains a dir.
var tabURL = (window.location.pathname).split('/');
var baseLink = "https://8chan.co/"+tabURL[1];
var inThread = false;

/**
 * Calls the init function as soon as everything from 8chan has been loaded.
 * This is required since 8chan's dev(s?) decided to generate their entire 
 * pages using javascript. I'm not a fan of that, but eh, if it works, sure.
 */
$(window).load(function() {
	init();
});

/**
 * Initializer function.
 * Calls everything and anything.
 */
function init()
{
	// Check at first if we're in a thread right now.
	initStatus_inThread();
  
	// Sets the CSS for the top section of the page.
	setCSS_ExpandAndTreeView();
	
	// Add extra options at the top of the page if we're in a thread.
	if (inThread) {
		addPageAnchors();
		add_threadToolbars();
	}
}

/**
 * Checks at the initial execution if the user's browser is in a thread.
 */
function initStatus_inThread() 
{
	return inThread = $.inArray('res', tabURL) != -1 ? true : false;
}

/**
 * Sets some CSS attributes on the "Expand all images" and "Tree view" links
 * on the right.
 */
function setCSS_ExpandAndTreeView()
{
	// Declare the array which includes all the css attributes that will be changed.
	var cssContainer = { display: "inline-block",
											 position: "absolute",
											 right: "15px",
											 float: "right",
										 };
	
	// Wraps those two things together.
	$("#expand-all-images, #treeview").wrapAll("<div class='side-options' />");
	
	// Adds the array of CSS attribs to the args for the container's css.
	$(".side-options").css(cssContainer);
}

/**
 * 
 */
function addPageAnchors()
{
	// Set the #bottom anchor.
	$("body").append("<div id='bottom'></div>").prepend("<div id='top'></div>");
}

/**
 * Adds the return, catalog and other links at the top and bottom of the thread, 
 * just like on 4chan!
 */
function add_threadToolbars()
{
	var tReturn = $('a:contains("[Return]")');
	var tCatalog = $('a:contains("[Catalog]")');
	var	tReturn_c = tReturn.clone();
	var tCatalog_c = tCatalog.clone();
	var cTop_class = "thread-toolbar-top";
	var	cBot_class = "thread-toolbar-bottom";
	
	$("div.blotter").next("hr").after("<div class='thread-toolbar-top'></div>");
	
	var container = $(".thread-toolbar-top"),
			appendItems = ['<span class="span-return">[<a class="link-return" href="/'+tabURL[1]+'/">Return</a>]</span>',
                            '<span class="span-catalog">[<a class="link-catalog" href="/'+tabURL[1]+'/catalog.html">Catalog</a>]</span>',
							'<span class="span-bottom">[<a class="link-bottom" href="#bottom">Bottom</a>]</span>',
                            '<span class="span-top">[<a class="link-top" href="#top">Top</a>]</span>'];
	
	container.css({
		display: "block",
		position: "relative",
		padding: "5px",
		color: "#81A2BE !important",
		fontSize: "13px"
	});
	
	container.append(appendItems[0], '<span class="span-catalog">[<a class="link-catalog" href="/'+tabURL[1]+'/catalog.html">Catalog</a>]</span>',
					  '<span class="span-bottom">[<a class="link-bottom" href="#bottom">Bottom</a>]</span>');
	
	container.after("<hr></hr>");
	
	container.find("span")
		.css({
		paddingRight: "3px"
	}).each(function() {
		$(this).find("a").css({
			color: "#81A2BE"
		});
	});
}









